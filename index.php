<?php

/**
 * This script checks the database of a TS3 server for new complaints. If new complaints are found,
 * the users of specific groups will be notified via poke. Also, all complaints are copied into an
 * external database to archive them.
 */


require_once('./vendor/autoload.php');
require_once('./configs/configure.php');
require_once('./includes/bugsnag.php');
require_once('./includes/classes/complaint.php');
require_once('./includes/classes/client.php');
require_once('./includes/classes/admin.php');
require_once('./includes/classes/troublemaker.php');
require_once('./includes/functions/data.php');
require_once('./includes/functions/connections.php');

$db_ts3 = database_connect(MYSQL_TS3_HOSTNAME, MYSQL_TS3_USERNAME, MYSQL_TS3_PASSWORD, MYSQL_TS3_DATABASE);
$fetched_trouble = troublemaker::fetch($db_ts3, data_read(DATA_FILENAME_TIMESTAMP));
$troublemakers = $fetched_trouble['troublemakers'];
$latest_timestamp = $fetched_trouble['latest_timestamp'];

if(count($troublemakers) > 0) // only if there are any new complaints
{
	$ts3query = ts3query_connect();
	$admins = admin::fetch($db_ts3, ADMIN_GROUP_IDS);
	$admins_online = 0;

	$clientlist = $ts3query->clientList();
	if(!$clientlist['success']){throw new Exception("Failed to get clientlist from TS3 query! ".$clientlist['errors']);}
	foreach($clientlist['data'] as $client)
	{
		if($admin = admin::find($admins, $client['client_database_id'], 'database_id'))
		{
			$admin->session_id = $client['clid'];
			$admin->poke($ts3query, $troublemakers);
			$admins_online++;
		}
	}

	foreach($troublemakers as $t)
	{
		$t->set_icon($ts3query);
	}

	if(ARCHIVE_COMPLAINTS)
	{
		$db_archive = database_connect(MYSQL_ARCHIVE_HOSTNAME, MYSQL_ARCHIVE_USERNAME, MYSQL_ARCHIVE_PASSWORD, MYSQL_ARCHIVE_DATABASE);
		troublemaker::archive_complaints($db_archive, $troublemakers, $admins_online);
	}

	data_write(DATA_FILENAME_TIMESTAMP, $latest_timestamp);

	foreach($admins as $a)
	{
		$a->mail(MAIL_ADMINS, $admins_online, $troublemakers);
	}
}

$ts3query = isset($ts3query) ? $ts3query : ts3query_connect();
troublemaker::remove_icons($db_ts3, $ts3query, $troublemakers);

?>