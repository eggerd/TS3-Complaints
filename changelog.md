2.2.0 - TBD
------------------
- Added `X-Content-IDs` header to mail notifications that can be used to identify complaints included in the message


2.1.0 - 29.07.2019
------------------
- At least PHP 7.0 is now required
- Updated configuration handler to version 2.4.0
- Added [Bugsnag](https://www.bugsnag.com/) integration for error tracking
- [ts3admin.class](https://github.com/par0noid/ts3admin.class) is now integrated using Composer


2.0.3 - 24.03.2019
------------------
- Fixed "*nickname is already in use*" error on servers running version 3.7.0, which was caused by the new naming convention for query users 
- Updated [ts3admin.class](https://github.com/par0noid/ts3admin.class) to version 1.0.2.5
- Fixed some spelling errors


2.0.2 - 13.10.2018
------------------
- Fixed a bug that caused mail notifications to be sent multiple times to each receiver


2.0.1 - 07.10.2018
------------------
- Fixed a bug that caused the script to fail, if it tried to get a list of permissions for a user that doesn't has any, because the query treats this as an error
- Fixed a bug that caused only the admin that connected first to be notified


2.0.0 - 26.08.2018
------------------
- **Issue** #18: Changed existing documentation to DocBlocks
- **Issue** #17: A users original icon is now restored, after all complaints against him are closed
- Code has been completely rewritten to be object oriented
- Only the server port is needed anymore and no longer the server ID
- Archiving complaints into a external database is now a optional feature
- Errors are now thrown instead of echoed
- Updated configuration handler to version 2.3.1


1.3.1 - 25.07.2017
------------------
- **Issue** #12: Removed unused value `ts3_server.complaints.complain_hash` from select statement
- **Issue** #15: Updated configuration handler to version 2.2.1
- **Issue** #15: Confidential settings have been exported to a untracked `secrets.php` file and will no longer be inserted during the build
- **Issue** #13: Updated ts3admin.class to version 1.0.2.1
- **Issue** #13: Changed parameter of `clientaddperm()` & `clientdelperm()` from integer to string, for better human readability


1.3.0 - 04.07.2017
------------------
- **Issue** #11: Project has been "englishified"
- **Issue** #9: Users with open complaints are now marked by an icon, that will be removed as soon as there are no more open complaints
- The version number is now located in the configuration file
- Nickname for query account can now be configured in the config file
- Removed error message `e004`, because it wasn't a real error and never used anyway
- Changelog is now written in Markdown
- Translated content of notification mail into English
- Data file has been moved to `/data/` and also been renamed to `evaluated.json`
- Replaced various white spaces with tabulators


1.2.4 - 17.01.2017
------------------
- **Issue** #4: Mails are no longer send to admins that are online and aren't configured to receive mails regardless
- **Issue** #1: It was possible that the data file could not be updated, thus complaints would be archived multiple times
- **Issue** #3: The amount of admins that are online is now archived as well


1.2.3 - 22.08.2016
------------------
- Mails can now be send to multiple different admins
- Mails can now be send, even if the corresponding admin is currently online
- Mails are now send, even if other admins are currently online


1.2.2 - 13.05.2016
------------------
- The nicknames and the complaint message are not utf8 decoded, so that they appear correctly in the external database


1.2.1 - 01.04.2016
--------------------
- the listing of nicknames in poke messages is now grouped correctly
- the listing of nicknames in poke messages is now separated by slashes and nicknames are enclosed by quotation marks
