# TS3-Complaints 

[![version](https://img.shields.io/badge/dynamic/json.svg?label=version&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F3272201%2Frepository%2Ftags&query=%24%5B0%5D.name&colorB=blue)](https://gitlab.com/eggerd/TS3-Complaints/-/releases)
[![pipeline status](https://gitlab.com/eggerd/TS3-Complaints/badges/master/pipeline.svg)](https://gitlab.com/eggerd/TS3-Complaints/-/pipelines) 
[![quality gate](https://sonarcloud.io/api/project_badges/measure?project=ts3-complaints&metric=alert_status)](https://sonarcloud.io/dashboard?id=ts3-complaints) 
[![maintainability](https://sonarcloud.io/api/project_badges/measure?project=ts3-complaints&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=ts3-complaints)

Teamspeak 3 has a feature that allows users to submit complaints against troublemakers. If a troublemaker receives a certain amount of complaints, that can be configured by the server owner, he will get banned for the configured amount of time. But this introduces multiple problems: On the one hand, setting the required amount of complaints too low could allow trolls to exploit the feature and get innocent users banned. But on the other hand, setting it too high might render it useless, if only a small group of users is affected by a troublemaker. 

Moreover, even if admins are present, they will not be notified about any complaints. Which, in my experience, is a problem, as most users use this feature like a ticket system and expect admins to react to those complaints.

This script provides the following features, in order to extend the Teamspeak 3 feature and meet those expectations:
- Notify users of certain groups via poke about new complaints
- Mark troublemakers with a configurable server icon, so that they are better visible in the tree (optional)
- Archive complaints into a separate database for future reference and to add comments (optional)
- Send mail notifications about new complaints (optional)

## Requirements

- PHP 7.0 or higher
- Teamspeak Server 3.2.0 or higher
- Read access to the database of the Teamspeak 3 server, for at least [those tables](https://gitlab.com/eggerd/TS3-Complaints/-/wikis/home#teamspeak-3-database)
- Access to the Teamspeak 3 query over Telnet, with a query account that has at least [these permissions](https://gitlab.com/eggerd/TS3-Complaints/-/wikis/home#teamspeak-3-query)
- Ability to schedule cronjobs
- [Composer](https://getcomposer.org/), to install dependencies
- Optionally, a database for archiving the complaints

## Installation

1. Download the [latest version](https://gitlab.com/eggerd/TS3-Complaints/-/releases)
1. Run `composer install` in the root directory of the project
1. Edit the [configuration](https://gitlab.com/eggerd/TS3-Complaints/-/wikis/home#available-settings) files in `configs/` according to your needs
1. If you want to use the feature to archive complaints, you also need to [set up the archive database](https://gitlab.com/eggerd/TS3-Complaints/-/wikis/home#archive-database)
1. Upload all files to your server
1. [Set up a cronjob](https://gitlab.com/eggerd/TS3-Complaints/-/wikis/home#cronjob) that executes the `index.php`

> The script accesses the Teamspeak 3 query over Telnet. Therefore, traffic between the script and the TS3 query is not encrypted! Due to this, it is recommended that the script is executed on the same machine as the TS3 server itself.

## Documentation

A documentation with more details can be found in the [wiki](https://gitlab.com/eggerd/TS3-Complaints/-/wikis) of the repository.

## Licensing

This software is available freely under the MIT License.  
Copyright (c) 2018 Dustin Eckhardt