<?php

// use par0noid\ts3admin as ts3admin; // commented out, because composer version doesn't have a namespace yet

/**
 * Represents a TS3 client with unseen complaints
 */
final class troublemaker extends client
{
	/**
	 * @var string $unique_id = the unique TS3 ID string
	 * @var integer $database_id = the TS3 database ID
	 * @var string $nickname = the currently used nickname
	 */
	public $unique_id, $database_id, $nickname;

	/** @var array $complaints = list of all unseen complaints */
	private $complaints;



	/**
	 * Creates a new troublemaker
	 *
	 * @param string $unique_id = the unique TS3 ID string
	 * @param integer $database_id = the TS3 database ID
	 * @param string $nickname = the currently used nickname
	 */
	public function __construct($unique_id, $database_id, $nickname)
	{
		$this->unique_id = $unique_id;
		$this->database_id = $database_id;
		$this->nickname = $nickname;
	}



	/**
	 * Adds a new complaint to the list of unseen complaints
	 *
	 * @param complaint $complaint = an unseen complaint against this client
	 * @return void
	 */
	public function add_complaint(complaint $complaint)
	{
		$this->complaints[] = $complaint;
	}



	/**
	 * Returns all unseen complaints
	 *
	 * @return complaint[] = list of all complaints
	 */
	public function get_complaints()
	{
		return $this->complaints;
	}



	/**
	 * Sets COMPLAINT_RECEIVER_ICON as the icon that will be shown behind the troublemaker on the
	 * TS3 client, if COMPLAINT_RECEIVER_ICON isn't FALSE
	 *
	 * @param ts3admin $ts3query = the connection to the TS3 query
	 * @return void
	 *
	 * @throws Exception Failed to set icon for client with ID '$DATABASE_ID'!
	 * @throws Exception Failed to get client permission for client with ID '$DATABASE_ID'!
	 */
	public function set_icon(ts3admin &$ts3query)
	{
		if(COMPLAINT_RECEIVER_ICON !== false) // only if this feature is enabled
		{
			$client_perms = $ts3query->clientPermList($this->database_id, true);
			$client_perms['empty'] = preg_match('/ErrorID: 1281/', implode('', $client_perms['errors'])); // error 1281 = empty database result
			if($client_perms['success'] || $client_perms['empty'])
			{
				if(!$client_perms['empty'])
				{
					foreach($client_perms['data'] as $perm)
					{
						if($perm['permsid'] == 'i_icon_id')
						{
							if($perm['permvalue'] != COMPLAINT_RECEIVER_ICON)
							{
								$existing_data = data_read(DATA_FILENAME_ICONS);
								!is_array($existing_data) ? $existing_data = array() : null;
								data_write(DATA_FILENAME_ICONS, $existing_data + array($this->database_id => $perm['permvalue']));
							}

							break;
						}
					}
				}

				if($ts3query->clientAddPerm($this->database_id, array('i_icon_id' => array(COMPLAINT_RECEIVER_ICON, 0)))['success'])
				{
					return;
				}

				throw new Exception("Failed to set icon for client with ID '".$this->database_id."'!");
			}

			throw new Exception("Failed to get client permission for client with ID '".$this->database_id."'!");
		}
	}



	/**
	 * Removes the icon from all clients that don't have open complaints anymore
	 *
	 * @param mysqli $db = the connection to the database of the TS3 server
	 * @param ts3admin $ts3query = the connection to the TS3 query
	 * @param troublemaker[] $troublemakers = list of troublemakers that have unseen complaints
	 * @return void
	 *
	 * @throws Exception Failed to remove icon from client with ID '$DATABASE_ID'!
	 * @throws Exception Failed to restore icon '$CACHED_ICON_ID' for client with ID '$DATABASE_ID'!
	 */
	public static function remove_icons(mysqli &$db, ts3admin &$ts3query, array &$troublemakers)
	{
		// fetch the TS3 database IDs of all clients that still have an icon, but no open complaints anymore
		$sql = $db->query('select id1 as database_id from servers as s, perm_client as pc left join complains as c on pc.id1 = c.complain_to_client_id where pc.server_id = s.server_id && s.server_port = "'.TS3_SERVER_PORT.'" && pc.perm_id = "i_icon_id" && pc.perm_value = "'.COMPLAINT_RECEIVER_ICON.'" && c.complain_to_client_id is null');
		if($sql->num_rows > 0)
		{
			while($row = $sql->fetch_object())
			{
				if(!troublemaker::find($troublemakers, $row->database_id))
				{
					if(!$ts3query->clientDelPerm($row->database_id, array('i_icon_id'))['success'])
					{
						throw new Exception("Failed to remove icon from client with ID '".$row->database_id."'!");
					}

					// restore previous icon, if necessary
					$cached_icons = data_read(DATA_FILENAME_ICONS);
					if(isset($cached_icons[$row->database_id]))
					{
						if(!$ts3query->clientAddPerm($row->database_id, array('i_icon_id' => array($cached_icons[$row->database_id], 0)))['success'])
						{
							throw new Exception("Failed to restore icon '".$cached_icons[$row->database_id]."' for client with ID '".$row->database_id."'!");
						}

						unset($cached_icons[$row->database_id]);
						data_write(DATA_FILENAME_ICONS, $cached_icons);
					}
				}
			}
		}
	}



	/**
	 * Fetches all open complaints from the database of the TS3 server that are newer than the given
	 * timestamp
	 *
	 * @param mysqli $db = the database connection to use for the query
	 * @param integer $newer_than = UNIX timestamp
	 * @return array {'troublemakers' => array{troublemaker}, 'latest_timestamp' => integer}
	 */
	public static function fetch(mysqli &$db, $newer_than)
	{
		$latest_timestamp = $newer_than;
		$troublemakers = array();

		$complains = $db->query('select
			complain_to_client_id,
			submitter.client_unique_id as submitter_uid,
			submitter.client_nickname as submitter_nickname,
			receiver.client_unique_id as receiver_uid,
			receiver.client_nickname as receiver_nickname,
			complain_message,
			complain_timestamp
		from complains
		left outer join clients as submitter on complain_from_client_id = submitter.client_id
		left outer join clients as receiver on complain_to_client_id = receiver.client_id
		where complains.server_id = 1 and complain_timestamp > '.$db->real_escape_string($newer_than).'
		order by complain_timestamp asc');

		while($row = $complains->fetch_object())
		{
			// if this is the first complaint against this receiver, during this execution
			if(!$receiver = troublemaker::find($troublemakers, $row->receiver_uid))
			{
				$receiver = new troublemaker($row->receiver_uid, $row->complain_to_client_id, $row->receiver_nickname);
				$troublemakers[] = $receiver;
			}

			$receiver->add_complaint(new complaint($row->submitter_uid, $row->submitter_nickname, $receiver, $row->complain_message, $row->complain_timestamp));
			$latest_timestamp = $row->complain_timestamp;
		}

		return array('troublemakers' => $troublemakers, 'latest_timestamp' => $latest_timestamp);
	}



	/**
	 * Saves all unseen complaints of all passed troublemakers into the archive database
	 *
	 * @param mysqli $db = the database connection to use for the query
	 * @param troublemaker[] $troublemakers = list of troublemakers which complaints should be archived
	 * @param integer $admins_online = amount of admins currently online
	 * @return void
	 */
	public static function archive_complaints(mysqli &$db, array &$troublemakers, $admins_online)
	{
		$complaints = array();
		foreach($troublemakers as $t)
		{
			$complaints = array_merge($complaints, $t->get_complaints());
		}

		complaint::archive_all($db, $complaints, $admins_online);
	}



	/**
	 * Returns the nickname of a troublemaker, prefixed with the amount of complaints
	 *
	 * @return string
	 */
	public function __toString()
	{
		return '['.count($this->complaints).'x] "'.$this->nickname.'"';
	}
}

?>
