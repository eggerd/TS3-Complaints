<?php

/**
 * Abstract class that represents a client on the TS3 server, like an admin or troublemaker
 */
abstract class client
{
	/**
	 * Searches an array of clients for a specific client, based on the value of the specified
	 * property
	 *
	 * @param client[] $haystack = an array of clients to search in
	 * @param string|integer $needle = the search value
	 * @param string $property = name of the property, that will be compared with $needle
	 * @param integer $limit = maximum amount of matches that should be returned
	 * @return false|static[]|static = a matching client ($limit = 1) or a list of matching clients ($limit > 1); FALSE if no matches are found
	 *
	 * @throws Exception Trying to find client by undefined property '$PROPERTY'!
	 */
	final public static function find(array &$haystack, $needle, $property = 'unique_id', $limit = 1)
	{
		// filter haystack with custom defined callback function
		$filtered = array_values(array_filter($haystack, function ($entry) use (&$needle, &$property)
		{
			if(!isset($entry->$property))
			{
				throw new Exception("Trying to find client by undefined property '".$property."'!");
			}

			return $entry->$property == $needle; // compare property of haystack entry with search value
		}));

		if(count($filtered) > 0)
		{
			return $limit == 1 ? $filtered[0] : array_slice($filtered, 0, $limit);
		}

		return false;
	}
}

?>
