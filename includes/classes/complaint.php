<?php

/**
 * Represents an unseen complaint
 */
final class complaint
{
	/**
	 * @var string $submitter_unique_id = the unique TS3 ID string of the submitter
	 * @var string $submitter_nickname = currently used nickname of the submitter
	 * @var troublemaker $receiver = the troublemaker of this complaint
	 * @var string $message = the complain message
	 * @var integer $timestamp = UNIX timestamp of the time the complaint was submitted
	 */
	public $submitter_unique_id, $submitter_nickname, $receiver, $message, $timestamp;



	/**
	 * Creates a new complaint
	 *
	 * @param string $submitter_unique_id = the unique TS3 ID string of the submitter
	 * @param string $submitter_nickname = currently used nickname of the submitter
	 * @param troublemaker $receiver = the troublemaker of this complaint
	 * @param string $message = the complain message
	 * @param integer $timestamp = UNIX timestamp of the time the complaint was submitted
	 */
	public function __construct($submitter_unique_id, $submitter_nickname, troublemaker $receiver, $message, $timestamp)
	{
		$this->submitter_unique_id = $submitter_unique_id;
		$this->submitter_nickname = $submitter_nickname;
		$this->receiver = $receiver;
		$this->message = $message;
		$this->timestamp = $timestamp;
	}



	/**
	 * Saves all passed complaints into the archive database
	 *
	 * @param mysqli $db = the database connection to use for the query
	 * @param complaint[] $complaints = list of all complaints that should be archived
	 * @param integer $admins_online = amount of admins that are currently online
	 * @return void
	 *
	 * @throws Exception Failed to archive complaints! $DATABASE_ERROR
	 */
	public static function archive_all(mysqli &$db, array &$complaints, $admins_online)
	{
		$sql_insert_values = array();
		foreach($complaints as $complaint)
		{
			// construct SQL insert value segment for this complaint
			$sql_insert_values[] = '("'.$complaint->submitter_unique_id.'",
				"'.$db->real_escape_string($complaint->submitter_nickname).'",
				"'.$complaint->receiver->unique_id.'",
				"'.$db->real_escape_string($complaint->receiver->nickname).'",
				"'.$db->real_escape_string($complaint->message).'",
				'.$complaint->timestamp.', '.$admins_online.')';
		}

		if(!$db->query('insert into complaint_history (submitter_uid, submitter_nickname,
		receiver_uid, receiver_nickname, message, unix_timestamp, mods_online) values '.implode(',', $sql_insert_values)))
		{
			throw new Exception("Failed to archive complaints! ".$db->error);
		}
	}
}

?>
