<?php

// use par0noid\ts3admin as ts3admin; // commented out, because composer version doesn't have a namespace yet

/**
 * Represents an admin of the TS3 server
 */
final class admin extends client
{
	/**
	 * @var integer $database_id = the TS3 database ID
	 * @var false|integer $session_id = the ID for the admins current connection; will be FALSE if offline
	 */
	public $database_id, $session_id = false;



	/**
	 * Creates a new admin
	 *
	 * @param integer $database_id = the TS3 database ID
	 */
	public function __construct($database_id)
	{
		$this->database_id = $database_id;
	}



	/**
	 * Pokes the admin to notify him about new complaints, if the admin is online
	 *
	 * @param ts3admin $ts3query = the connection to the TS3 query
	 * @param troublemaker[] $troublemakers = list of all troublemakers with new complaints
	 * @return void
	 *
	 * @throws Exception Failed to poke admin with ID '$DATABASE_ID'!
	 */
	public function poke(ts3admin &$ts3query, array &$troublemakers)
	{
		if($this->session_id) // only if online
		{
			if(!$ts3query->clientPoke($this->session_id, 'New Complaints: '.implode(' / ', $troublemakers))['success'])
			{
				throw new Exception("Failed to poke admin with ID '".$this->database_id."'!");
			}
		}
	}



	/**
	 * Notifies the admin via mail about new complaints, if he is one of the recipients on the
	 * configured MAIL_ADMINS list
	 *
	 * @param array $mail_list @see MAIL_ADMINS in configuration
	 * @param integer $admins_online = amount of admins that are currently online
	 * @param troublemaker[] $troublemakers = list of all troublemakers
	 * @return void
	 *
	 * @throws Exception Failed to sent mail notification to '$RECEIVER_MAIL'!
	 */
	public function mail(array $mail_list, $admins_online, array $troublemakers)
	{
		$receiver = array_search($this->database_id, array_column($mail_list, 'dbid'));
		if($receiver !== false)
		{
			// determine if mail should be send to this admin
			$send_mail = true;
			if($this->session_id && !$mail_list[$receiver]['always'])
			{
				$send_mail = false;
			}

			if($send_mail)
			{
				// construct mail content
				$tuples = array();
				$content = 'Admins online: '.$admins_online.'<br>';
				foreach ($troublemakers as $t)
				{
					$content .= '<br><b>'.$t->nickname.'</b><br>';
					foreach ($t->get_complaints() as $complaint)
					{
						$content .= '>> '.$complaint->submitter_nickname.': "'.$complaint->message.'"<br>';
						$tuples[] = $complaint->timestamp.':'.$complaint->submitter_unique_id;
					}
				}

				$header  = "MIME-Version: 1.0\r\nContent-type: text/html; charset=utf-8\r\nFrom: ".MAIL_SUBMITTER."\r\nX-Content-IDs: ".json_encode($tuples);
				if(!mail($mail_list[$receiver]['mail'], 'TS3: New Complaints', $content, $header))
				{
					throw new Exception("Failed to sent mail notification to '".$mail_list[$receiver]['mail']."'!");
				}
			}
		}
	}



	/**
	 * Fetches all clients from the TS3 database that have one of the admin groups assigned to them
	 *
	 * @param mysqli $db = the connection to the database of the TS3 server
	 * @param integer[] $admin_group_ids = list with the numeric IDs of all admin groups
	 * @return static[] = list of all admins
	 *
	 * @throws Exception Failed to fetch any admins from the database of the TS3 server!
	 */
	public static function fetch(mysqli &$db, array $admin_group_ids)
	{
		$admins = array();
		$sql = $db->query('select id1 from group_server_to_client as gsc, servers as s where gsc.group_id in ('.implode(',', $admin_group_ids).') && gsc.id1 != 1 && (gsc.server_id = 0 || gsc.server_id = s.server_id && s.server_port = '.TS3_SERVER_PORT.')');
		if($sql->num_rows > 0)
		{
			while($row = $sql->fetch_object())
			{
				$admins[] = new admin($row->id1);
			}

			return $admins;
		}

		throw new Exception("Failed to fetch any admins from the database of the TS3 server!");
	}
}

?>
