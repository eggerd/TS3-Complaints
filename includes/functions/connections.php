<?php

// use par0noid\ts3admin as ts3admin; // commented out, because composer version doesn't have a namespace yet

/**
 * Establishes a connection to the TS3 query, login & change the nickname
 *
 * @return ts3admin
 *
 * @throws Exception Failed to select the virtual TS3 server with port '$TS3_SERVER_PORT'!
 * @throws Exception Failed to establish connection to the TS3 query at '$TS3_IP:$TS3_QUERY_PORT'!
 */
function ts3query_connect()
{
	$query = new ts3admin(TS3_IP, TS3_QUERY_PORT);
	if($query->connect()['success'])
	{
		$query->login(TS3_USER, TS3_PASSWORD);
		if($query->selectServer(TS3_SERVER_PORT)['success'])
		{
			if($query->whoAmI()['data']['client_nickname'] !== TS3_NICKNAME)
			{
				if(!$query->clientUpdate(array('client_nickname' => TS3_NICKNAME))['success'])
				{
					trigger_error("Failed to change nickname to '".TS3_NICKNAME."'", E_USER_NOTICE);
				}
			}

			return $query;
		}

		throw new Exception("Failed to select the virtual TS3 server with port '".TS3_SERVER_PORT."'!");
	}

	throw new Exception("Failed to establish connection to the TS3 query at '".TS3_IP.":".TS3_QUERY_PORT."'!");
}



/**
 * Establishes a connection to the specified database
 *
 * @param string $hostname = IP or FQDN of the database server
 * @param string $username = username to login with
 * @param string $password = password for the username
 * @param string $database = name of the database to use
 * @return mysqli = the database connection
 *
 * @throws Exception Failed to connect to database '$DATABASE@$HOSTNAME'! $DATABASE_ERROR
 */
function database_connect($hostname, $username, $password, $database)
{
	$db = new mysqli($hostname, $username, $password, $database);
	if($db->connect_error){throw new Exception("Failed to connect to database '".$database."@".$hostname."'! ".$db->connect_error);}

	return $db;
}

?>