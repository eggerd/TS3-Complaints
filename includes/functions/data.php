<?php

/**
 * Reads and returns the content of the specified file, that has to be located inside the data
 * directory
 *
 * @param string $filename = name of a file inside the data directory, that should be read
 * @return mixed = json_decode()
 */
function data_read($filename)
{
	$file_path = realpath(dirname(__FILE__).'/../../').'/data/'.$filename;
	if(!is_readable($file_path))
	{
		data_write($filename, 0);
		return 0;
	}

	$file = fopen($file_path, 'r');
	$file_data = json_decode(fread($file, filesize($file_path) + 1), true);
	fclose($file);

	return $file_data;
}



/**
 * Writes the passed data into the specified data file, overwriting existing data. If the data
 * directory and/or file does not exist, it will attempt to create them
 *
 * @param string $filename = name of a file inside the data directory, that the data will be written into
 * @param mixed $data = the data that will be written into the file
 * @return void
 *
 * @throws Exception Failed to create data directory!
 * @throws Exception Failed to write into data file '$FILENAME'!
 */
function data_write($filename, $data)
{
	$data_path = realpath(dirname(__FILE__).'/../../').'/data/';
	if(!is_dir($data_path) && !mkdir($data_path, 0775))
	{
		throw new Exception("Failed to create data directory!");
	}

	$file = fopen($data_path.$filename, 'w+');
	if(fwrite($file, json_encode($data)) === false)
	{
		throw new Exception("Failed to write into data file '".$filename."'!");
	}
}

?>