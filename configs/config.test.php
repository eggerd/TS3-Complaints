<?php

/**
 * Configuration for your test environment.
 * Only define settings that have to be different from the production environment!
 */

config::set('MYSQL_TS3_DATABASE', 'test_ts3_server');
config::set('MYSQL_ARCHIVE_DATABASE', 'test_ts3_admin');
config::set('ADMIN_GROUP_IDS', [2]);

?>