<?php

config::set('VERSION', '2.2.0');


// ***** Teamspeak Query *****

// Port of the TS3 server
config::set('TS3_SERVER_PORT', 9987); // :integer

// IP address or FQDN of the TS3 server
config::set('TS3_IP', '127.0.0.1'); // :string

// Port number of the TS3 query
config::set('TS3_QUERY_PORT', 10011); // :integer

// Username of the query account that will be used for pokes and setting/removing icons (if enabled)
config::set(config::secret('TS3_USER')); // :string

// Password for the query account
config::set(config::secret('TS3_PASSWORD')); // :string

// Username that will be used when connecting to the TS3 query - will be shown on poke messages and
// server logs
config::set('TS3_NICKNAME', 'Server [Complaints]'); // :string



// ***** General *****

// List containing the numeric ID of each group whose users will be notified on new complaints. Can
// be found in the group manager (Permissions -> Server Groups) behind every group name
config::set('ADMIN_GROUP_IDS', [2, 12]); // :array(:integer)

// The numeric ID of the TS3 icon that will be assigned to users with open complaints. Can be found
// in the icon manager (Right click server name in the tree -> Show icons) when hovering over a icon.
// This ID has to be converted into a 32-bit signed integer! If FALSE is used, this feature will be
// disabled
config::set('COMPLAINT_RECEIVER_ICON', '-1357759298'); // :integer | :boolean

// The mail address that will be set as submitter for mail notifications. Only required if any
// `MAIL_ADMIN` are configured
config::set('MAIL_SUBMITTER', 'noreply.ts3complaints@dustin-eckhardt.de'); // :string

// List of admins that will also be notified by mail
config::set('MAIL_ADMINS', [
	/* array
	 * (
	 * 		'dbid' => :integer 		// the TS3 database ID of this admin
	 * 		'mail' => :string 		// the mail address notifications will be send to
	 * 		'always' => :boolean 	// if mails should always be send (TRUE) or only if this admin is offline (FALSE)
	 * )
	 */
	['dbid' => 2, 'mail' => 'eggerd@online.de', 'always' => false]
]);



// ***** Storage *****

// Name of the file that will contain the timestamp of the last evaluated complaint
config::set('DATA_FILENAME_TIMESTAMP', 'evaluated.json'); // :string

// Name of the file that will contain the IDs of all user icons which have been temporarily replaced
// by the `COMPLAINT_RECEIVER_ICON`
config::set('DATA_FILENAME_ICONS', 'icons.json'); // :string



// ***** Teamspeak Database *****

// IP or FQDN of the TS3 database server
config::set('MYSQL_TS3_HOSTNAME', 'localhost'); // :string

// Username to login with - a list of required permissions can be found in the projects wiki at
// https://gitlab.com/eggerd/TS3-Complaints/-/wikis/home#teamspeak-3-database
config::set(config::secret('MYSQL_TS3_USERNAME')); // :string

// Password for the user
config::set(config::secret('MYSQL_TS3_PASSWORD')); // :string

// Name of the database that is used by the TS3 server
config::set('MYSQL_TS3_DATABASE', 'ts3_server'); // :string



// ***** Archive Database *****

// If enabled (TRUE), complaints will be archived into the database configured below
config::set('ARCHIVE_COMPLAINTS', true); // :boolean

// IP or FQDN of the archive database server. Only required if `ARCHIVE_COMPLAINTS` is enabled
config::set('MYSQL_ARCHIVE_HOSTNAME', 'localhost'); // :string

// Username to login with. Only required if `ARCHIVE_COMPLAINTS` is enabled
config::set(config::secret('MYSQL_ARCHIVE_USERNAME')); // :string

// Password for the user. Only required if `ARCHIVE_COMPLAINTS` is enabled
config::set(config::secret('MYSQL_ARCHIVE_PASSWORD')); // :string

// Name of the database to use. Only required if `ARCHIVE_COMPLAINTS` is enabled
config::set('MYSQL_ARCHIVE_DATABASE', 'ts3_admin'); // :string



/***** Error Tracking *****/

// API key for the Bugsnag project (leave empty to disable tracking)
config::set('BUGSNAG_API_KEY', ''); // :string

// List of environment names that will be tracked by Bugsnag
config::set('BUGSNAG_TRACK_ENVIRONMENTS', ['prod', 'test']); // :array

?>
